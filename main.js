// On document ready
$(function () {
    // Variables initialisation and declaration
    var $container = $('#colorPicker');
    var $image = $container.find('> img');
    var $picker = $container.find('> div');

    var duration = 500;
    var lastColor = $image.attr('src');

    // Toggle on button click
    $image.click(function (e) {
        $picker.fadeToggle(duration);

        e.stopPropagation();
    });

    // Fade out on click out of the button if the clicked element is a children of #colorPicker
    // or on keypress (backspace, enter, esc, space, del)
    $(document)
        .click(function (e) {
            if ($picker.is(':visible') && (!$(e.target).is($picker) && !$(e.target.parentNode).is($picker))) {
                $picker.fadeOut(duration);
            }
        })

        .keydown(function (e) {
            if ($picker.is(':visible')) {
                $.each([8, 13, 27, 32, 46], function (key, value) {
                    if (value === e.keyCode) {
                        $picker.fadeOut(duration);

                        return false;
                    }
                });
            }
        });

    // Change color on mouse hover and click
    $picker.find('img:not(:first)')
        .mouseenter(function () {
            $image.attr('src', $(this).attr('src'));
        })

        .mouseleave(function () {
            $image.attr('src', lastColor);
        })

        .click(function () {
            lastColor = $image.attr('src');
            $image.attr('src', $(this).attr('src'));
        });
});
